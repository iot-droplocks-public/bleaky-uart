# Bleaky UART

Communicate with BLE UART devices (i.e. IoT Droplocks) using the BLEAK library for Python. BLE on Windows 10!

## Running

1. Setup a `venv` and activate it
1. `pip install bleak pillow pycryptodome`

There are three scripts that do various jobs:

### `bleaky_uart.py`

1. Set the MAC address of the BLE you want to talk to
1. Do whatever your want... the example steals a fingerprint

### `bleaky_serial.py`

1. Set the MAC address of the BLE you want to talk to
1. Provide two 8-digit hex values for serial and key and run against a fresh device to offline-enroll it.

### `bleaky_dfu.py`

1. Set the MAC address of the BLE you want to talk to
1. Provide two 8-digit hex values for serial and key of the device you're talking to
1. Running the script will enable DFU mode on the target for three minutes. Nordic's DFU Android app will then be able to find and program it.