#!/usr/bin/env python3
# Take over a fresh lock and give it our own serial number

import asyncio
import datetime
import time
import struct
import sys
from codecs import decode
from Crypto.Cipher import AES
from bleak import BleakClient
from bleak import _logger as logger
from bleak.uuids import uuid16_dict

# This is static for fresh locks
SERIAL = decode('a5ce965a', 'hex')[::-1]
NEW_SERIAL = decode(sys.argv[1], 'hex')
NEW_KEY = decode(sys.argv[2], 'hex')


# The difference is...>|< here!
UART_TX_UUID = "6e400002-b5a3-f393-e0a9-e50e24dcca9e" # Nordic NUS characteristic for TX
UART_RX_UUID = "6e400003-b5a3-f393-e0a9-e50e24dcca9e" # Nordic NUS characteristic for RX

data_received = bytes()

DEBUG = True

def notification_handler(sender, data):
    """Simple notification handler which prints the data received."""
    global data_received
    data_received += data
    if DEBUG:
        print(f"{datetime.datetime.utcnow()} Received {len(data):02} bytes: {data.hex()}")

def checksum(data, check=False):
    cks = 0xff
    if check:
        return sum(data[:-2]).to_bytes(2, 'little') == data[-2:]
    else:
        cks = sum(data) & 0xffff
        return bytes(data) + cks.to_bytes(2, 'little')


def tappcrypt_key_expand(rand, serial):
    print(f"Random: 0x{rand.hex()}, Serial: 0x{serial.hex()}")
    intermediate = bytes([(r + s) & 0xff for r,s in zip(rand,serial)])
    print(f"Intermediate: 0x{intermediate.hex()}")
    key = bytes()
    for r in range(4):
        key += bytes([rand[r] ^ intermediate[0]])
        key += bytes([rand[r] | intermediate[1]])
        key += bytes([rand[r] & intermediate[2]])
        key += bytes([rand[r] ^ intermediate[3]])
    # Reverse and return
    return bytes(key[::-1])


async def ble_send(client, data):
    while data:
        if DEBUG:
            print(f"{datetime.datetime.utcnow()} Sending {len(data):02} bytes:  {data[:client.mtu_size-3].hex()}")
        await client.write_gatt_char(UART_TX_UUID, data[:client.mtu_size-3])
        data = data[client.mtu_size-3:]

# We need to:
#  1. Collect some bytes to send
#  2. Receive some bytes back
async def run(address, loop):

    async with BleakClient(address, loop=loop) as client:

        #wait for BLE client to be connected
        print(f"Connected: {client.is_connected}")

        #wait for data to be sent from client
        await client.start_notify(UART_RX_UUID, notification_handler)

        print("Started notify")

        print(f"MTU: {client.mtu_size}")

        global data_received

        # Get lock version
        await ble_send(client, bytes([0x55, 0xaa, 0x45, 0x01, 0x00, 0x00, 0x45, 0x01]))
        while len(data_received) < 13:
            await asyncio.sleep(0.05)
            # Wait for the data
        hw_version, fw_version = struct.unpack('>HH', data_received[7:11])
        data_received = data_received[13:]
        print(f"Lock HW: {hex(hw_version)}, FW: 0x{fw_version:04x}")

        # Get random number
        await ble_send(client, bytes([0x55, 0xaa, 0x01, 0x03, 0x00, 0x00, 0x03, 0x01]))
        while len(data_received) < 13:
            await asyncio.sleep(0.05)
            # Wait for the data
        randNumber = data_received[7:11][::-1]
        data_received = data_received[13:]
        #SERIAL = decode(sys.argv[1], 'hex')
        key = tappcrypt_key_expand(randNumber, SERIAL)
        print(f"Key: 0x{key.hex()}")

        # Generate the verify message
        verify = randNumber[::-1] + bytes([0x0c] * 0x0c)
        print(f"To encrypt: 0x{verify.hex()}")
        cipher = AES.new(key, AES.MODE_ECB)
        data = cipher.encrypt(verify)[:12]
        print(f"Verify: 0x{data.hex()}")
        # Send the verify message
        to_send = checksum(bytes([0x55, 0xaa, 0x02, 0x03, 0x0c, 0x00]) + data)
        await ble_send(client, to_send)
        while len(data_received) < 9:
            await asyncio.sleep(0.05)
            # Wait for the data
        print(data_received.hex())
        if data_received[-3]:
            print("WIN")
        else:
            print("FAIL")
        data_received = data_received[9:]

        # Do initial pairing
        # The initial pairing key is 0xae, 0x95, 0x66, 0xc7
        first_pair = checksum(bytes([0x55, 0xaa, 0x81, 0x01, 0x04, 0x00, 0xae, 0x95, 0x66, 0xc7])) + bytes([0x04] * 0x04)
        first_pair_e = cipher.encrypt(first_pair)
        await ble_send(client, first_pair_e)
        while len(data_received) < 16:
            await asyncio.sleep(0.05)
            # Wait for the data
        first_resp = cipher.decrypt(data_received[:16])
        print("FIRST_PAIRING:" + first_resp.hex())
        if first_resp[6]:
            print("WIN")
        else:
            print("FAIL")
        data_received = data_received[16:]

        # Do boot?
        boot_msg = checksum(bytes([0x55, 0xaa, 0x92, 0x01, 0x08, 0x00]) + NEW_KEY + NEW_SERIAL)
        await ble_send(client, boot_msg)
        while len(data_received) < 16:
            await asyncio.sleep(0.05)
            # Wait for the data
        boot_resp = cipher.decrypt(data_received[:16])
        print("BOOT:" + boot_resp.hex())
        if boot_resp[6]:
            print("WIN")
            print(f'Lock has been booted with serial 0x{NEW_SERIAL.hex()} and key 0x{NEW_KEY.hex()}')
        else:
            print("FAIL")
        data_received = data_received[16:]

        

        return

if __name__ == "__main__":

    #this is MAC of our BLE device
    address = (
        "F9:2A:A7:01:9C:05"
    )
    print("Starting")
    loop = asyncio.new_event_loop()
    loop.run_until_complete(run(address, loop))