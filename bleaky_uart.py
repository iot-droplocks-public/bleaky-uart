#!/usr/bin/env python3
# Hacked together with https://github.com/coyt/PythonNUS/blob/master/main.py as inspiration
# Uses BLEAK library for BLE on Windows 10, yay

import platform
import logging
import asyncio
import datetime
import time
import struct
from bleak import BleakClient
from bleak import _logger as logger
from bleak.uuids import uuid16_dict
from PIL import Image

"""
TODO

- Handle packets more gracefully (variable size, responses with payloads)
- Do Fingerprint Download
  - Sends GET_IMAGE command
  - Gets response
  - Sends UP_IMAGE_CODE command
  - Gets response
  - Gets 2nd response containing fingerprint image
- On the device:
  - Need to find either 6.4K or 24.5K or RAM(!)
    - Or do something clever with a ring-buffer or repeated commands
"""

# The difference is...>|< here!
UART_TX_UUID = "6e400002-b5a3-f393-e0a9-e50e24dcca9e" # Nordic NUS characteristic for TX
UART_RX_UUID = "6e400003-b5a3-f393-e0a9-e50e24dcca9e" # Nordic NUS characteristic for RX

# The bytes sent to get the reader to capture a print
CMD_GET_FINGER = [0x55, 0xaa, 0x00, 0x00, 0x20, 0x00, 0x02, 0x00] + [0x00] * 16
CMD_UPLOAD_FINGER = []

data_received = bytes()

DEBUG = False

def notification_handler(sender, data):
    """Simple notification handler which prints the data received."""
    global data_received
    data_received += data
    if DEBUG:
        print(f"{datetime.datetime.utcnow()} Received {len(data)} bytes: {data.hex():40} [{data.decode('utf-8', errors='ignore'):20}]")

def checksum(data, check=False):
    cks = 0xff
    if check:
        return sum(data[:-2]).to_bytes(2, 'little') == data[-2:]
    else:
        data += [0] * max(0, (24 - len(data)))
        cks = sum(data) & 0xffff
        return bytes(data) + cks.to_bytes(2, 'little')

async def ble_send(client, data):
    while data:
        await client.write_gatt_char(UART_TX_UUID, data[:client.mtu_size-3])
        data = data[client.mtu_size-3:]

# We need to:
#  1. Collect some bytes to send
#  2. Receive some bytes back
async def run(address, loop):

    async with BleakClient(address, loop=loop) as client:

        #wait for BLE client to be connected
        print(f"Connected: {client.is_connected}")

        #wait for data to be sent from client
        await client.start_notify(UART_RX_UUID, notification_handler)

        print("Started notify")

        print(f"MTU: {client.mtu_size}")

        global data_received

        # Test connection
        to_send = checksum([0x55, 0xaa, 0x00, 0x00, 0x01, 0x00, 0x00])
        if DEBUG: print(f"Sending connection test [{len(to_send)} bytes]: {to_send.hex()}")
        await ble_send(client, to_send)
        while len(data_received) < 26:
            await asyncio.sleep(0.05)
            # Wait for the data
        result, = struct.unpack('H', data_received[8:10])
        if result == 0:
            print("MCU->FPR comms OK")
        else:
            print(f"MCU->FPR comms fail: {hex(result)}")
        data_received = data_received[26:]

        # Get Device info first to make sure it's working
        to_send = checksum([0x55, 0xaa, 0x00, 0x00, 0x04, 0x00, 0x00])
        if DEBUG: print(f"Sending device info query [{len(to_send)} bytes]: {to_send.hex()}")
        await ble_send(client, to_send)
        while len(data_received) < 74:
            await asyncio.sleep(0.05)
            # Wait for the data
        info = data_received[36:-2].decode().rstrip('\x00')
        print(f"Device info: '{info}'")
        data_received = data_received[74:]

        # Keep asking for a fingerprint until the reader reports acquisition
        got_finger = False
        while not got_finger:
            print("Attempting to capture fingerprint", end="\r")
            await asyncio.sleep(0.5)
            to_send = checksum([0x55, 0xaa, 0x00, 0x00, 0x20, 0x00, 0x02, 0x00])
            if DEBUG: print(f"Sending fingerprint capture request [{len(to_send)} bytes]: {to_send.hex()}")
            await ble_send(client, to_send)
            while len(data_received) < 26:
                await asyncio.sleep(0.05)
                # Wait for the data
            got_finger = data_received[8] == 0
            data_received = data_received[26:]
        print("\nFingerprint acquired by reader   ")

        # Ask the reader to upload the print to us
        to_send = checksum([0x55, 0xaa, 0x00, 0x00, 0x22, 0x00, 0x01, 0x00, 0x00])
        if DEBUG: print(f"Requesting fingerprint upload [{len(to_send)} bytes]: {to_send.hex()}")
        await ble_send(client, to_send)
        while len(data_received) < 26:
            await asyncio.sleep(0.05)
        w, h = struct.unpack('HH', data_received[10:14])
        bytes_expected = w * h
        print(f"Image dimensions: {w} x {h} ({bytes_expected} bytes)")
        data_received = data_received[26:]
        bytes_received = 0
        image = bytes()
        time_start = time.perf_counter_ns()
        while bytes_received < bytes_expected:
            print(f"Received {bytes_received:>5} / {bytes_expected} bytes", end="\r")
            # Get at least the start of a packet
            while len(data_received) < 26:
                await asyncio.sleep(0.05)
            pkt_len = struct.unpack('H', data_received[6:8])[0] + 10
            #print(f"Incoming packet is {pkt_len} bytes")
            img_frag, = struct.unpack('H', data_received[10:12])
            #print(f"This image fragment is {img_frag} bytes")
            while len(data_received) < pkt_len:
                await asyncio.sleep(0.05)
            bytes_received += img_frag
            image += data_received[12:12+img_frag]
            data_received = data_received[pkt_len:]
        #print(f"Got {bytes_received} bytes total!")
        #print(image.hex())
        time_end = time.perf_counter_ns()
        seconds = int((time_end - time_start) * 1e-9)
        bitrate = int((bytes_received * 8) / seconds)
        print(f"Received {bytes_received:>5} / {bytes_expected} bytes in {seconds} seconds ({bitrate}bps)")

        # Show the image :)
        print("Got your finger!")
        img = Image.frombytes('L', (w, h), image, "raw", 'L', 0, 1)
        img.show(title="Got your finger!")
        return


if __name__ == "__main__":

    #this is MAC of our BLE device
    address = (
        "FA:00:79:73:33:CE"
    )

    loop = asyncio.new_event_loop()
    loop.run_until_complete(run(address, loop))